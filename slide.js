'use strict';

//require
var Ajv = require('ajv');
var ajv = Ajv({
  verbose: true,
  allErrors: true
    //v5: true  //enable v5 proposal of JSON-schema standard
}); // options can be passed, e.g. {allErrors: true}

//build schema
const objectid = {
  type: 'string',
  maxLength: 24,
  minLength: 24
};
//keyword for checking ids of all revisions in order to blame duplicated ids
//it will create an error if an id is used twice and no other errors occured
ajv.addKeyword('validateRevisionIds', {
  errors: true,
  async: false,
  validate: function myValidation(value, dataObject) {
    if (!value)
      return true;

    if (dataObject.revisions === undefined || Object.prototype.toString.call(dataObject.revisions) !== '[object Array]')
      return true;

    if (myValidation.errors === null)
      myValidation.errors = [];

    let usedIds = {};
    dataObject.revisions.forEach((revision) => {
      if (usedIds[revision.id]) {
        myValidation.errors.push({
          keyword: 'validateRevisionIds',
          message: 'Revision id ' + revision.id + ' is used twice!',
          params: {
            keyword: 'validateRevisionIds',
            key: 'id'+revision.id
          }/*,
          dataPath: '.maxPoints',
          schemaPath: '#/validateMaxPoints',
          schema: true*/
        });
        //console.log('Added error for revid ' + revision.id);
      }
      else {
        usedIds[revision.id] = true;
      }
    });

    if (myValidation.errors.length === 0)
      return true;

    return false;
  }
});
const contributer = {
  type: 'object',
  properties: {
    id: objectid,
    name: {
      type: 'string'
    }
  },
  required: ['id']
};
const slideRevision = {
  type: 'object',
  properties: {
    id: { //increment with every new revision
      type: 'number',
      minimum: 1
    },
    title: {
      type: 'string'
    },
    timestamp: {
      type: 'string'
    },
    content: {
      type: 'string'
    },
    user: objectid,
    parent: {
      type: 'object'
    }, //ObjectId or Number or both
    popularity: {
      type: 'number',
      minimum: 0
    },
    comment: {
      type: 'string'
    },
    note: {
      type: 'string'
    },
    license: {
      type: 'string',
      enum: ['CC0', 'CC BY', 'CC BY-SA']
    },
    translation: {
      type: 'object',
      properties: {
        status: {
          type: 'string',
          enum: ['original', 'google', 'revised']
        },
        translator: objectid,
        source: {
          type: 'object'
        }
      }
    },
    tags: {
      type: 'array',
      items: {
        type: 'string'
      }
    },
    media: {
      type: 'array',
      items: objectid
    },
    dataSources: {
      type: 'array',
      items: objectid
    }
  },
  required: ['id', 'timestamp', 'user', 'license']
};
const slide = {
  type: 'object',
  properties: {
    user: objectid,
    description: {
      type: 'string'
    },
    language: {
      type: 'string'
    },
    translation: {
      type: 'object'
    },
    deck: objectid,
    position: {
      type: 'number',
      minimum: 1
    },
    timestamp: {
      type: 'string'
    },
    revisions: {
      type: 'array',
      items: slideRevision
    },
    contributers: {
      type: 'array',
      items: {
        oneOf: [
          contributer
        ]
      }
    },
    tags: {
      type: 'array',
      items: {
        type: 'string'
      }
    }
  },
  required: ['user', 'deck', 'timestamp'],
  validateRevisionIds: true
};

//export
module.exports = ajv.compile(slide);

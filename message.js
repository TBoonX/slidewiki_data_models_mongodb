'use strict';

//require
var Ajv = require('ajv');
var ajv = Ajv({
  verbose: true,
  allErrors: true
    //v5: true  //enable v5 proposal of JSON-schema standard
}); // options can be passed, e.g. {allErrors: true}

//build schema
const objectid = {
  type: 'string',
  maxLength: 24,
  minLength: 24
};
const message = {
  type: 'object',
  properties: {
    sender: objectid,
    receiver: objectid,
    title: {
      type: 'string'
    },
    content: {
      type: 'string'
    },
    timestamp: {
      type: 'string'
    },
    type: {
      type: 'string',
      enum: ['pm', 'editorship']
    }
  },
  required: ['sender', 'receiver', 'content', 'timestamp', 'type']
};

//export
module.exports = ajv.compile(message);

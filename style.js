'use strict';

//require
var Ajv = require('ajv');
var ajv = Ajv({
  verbose: true,
  allErrors: true
    //v5: true  //enable v5 proposal of JSON-schema standard
}); // options can be passed, e.g. {allErrors: true}

//build schema
const objectid = {
  type: 'string',
  maxLength: 24,
  minLength: 24
};
const style = {
  type: 'object',
  properties: {
    user: objectid,
    name: {
      type: 'string'
    },
    scss: {
      type: 'object',
      properties: {
        varfunc: {
          type: 'string'
        },
        content: {
          type: 'string'
        }
      }
    },
    css: {
      type: 'string'
    },
    timestamp: {
      type: 'string'
    },
    comment: {
      type: 'string'
    },
    parent: {
      type: 'object'
    }
  },
  required: ['user', 'timestamp']
};

//export
module.exports = ajv.compile(style);

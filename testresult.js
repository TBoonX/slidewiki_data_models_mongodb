'use strict';

//require
var Ajv = require('ajv');
var ajv = Ajv({
  verbose: true,
  allErrors: true
    //v5: true  //enable v5 proposal of JSON-schema standard
}); // options can be passed, e.g. {allErrors: true}

//build schema
const objectid = {
  type: 'string',
  maxLength: 24,
  minLength: 24
};
const testresult = {
  type: 'object',
  properties: {
    test: objectid,
    question: {
      type: 'number',
      minimum: 1
    },
    answer: {
      type: 'string'
    },
    explanation: {
      type: 'string'
    },
    isRight: {
      type: 'boolean'
    },
    user: objectid
  },
  required: ['test', 'question', 'user']
};

//export
module.exports = ajv.compile(testresult);

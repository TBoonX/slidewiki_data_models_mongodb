// example unit tests
/* eslint dot-notation: 0 */
'use strict';

describe('Model Comment', () => {
  let expect, CommentModel = require('../comment.js'), co;

  //data
  const wrong_comment = {
    blub: true,
    user: false,
    item: '{}',
    title: 12,
    text: true,
    timestamp: null
  };
  const slightlyWrongComment = {
    user: '112233445566778899101112',
    item: {
      kind: 'SLIDEE',
      ref: {
        id: '112233445566778899101112',
        revision: false
      }
    }
  };
  const correctComment = {
    user: '112233445566778899101112',
    item: {
      kind: 'slide',
      ref: {
        id: '112233445566778899101112',
        revision: 1
      }
    },
    timestamp: (new Date()).toString(),
    title: 'title',
    text: 'Content'
  };

  //get modules
  beforeEach(() => {
    require('chai').should();
    expect = require('chai').expect;
    co = require('../common.js');
  });

  context('when validating wrong comments it', () => {
    it('should return an error with the wrong elements', () => {
      const success = CommentModel(wrong_comment);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(CommentModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong.user).to.not.equal(undefined);
      expect(errors.wrong.timestamp).to.not.equal(undefined);
      expect(errors.wrong.item).to.not.equal(undefined);
    });

    it('should return an error with the wrong elements #2', () => {
      const success = CommentModel(slightlyWrongComment);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(CommentModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.missing.timestamp).to.not.equal(undefined);
      expect(errors.wrong['item.kind']).to.not.equal(undefined);
      expect(errors.wrong['item.ref.revision']).to.not.equal(undefined);
    });
  });

  context('when validating correct comments', () => {
    it('should validate without errors', () => {
      const success = CommentModel(correctComment);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(CommentModel.errors);

      //console.log(errors);

      expect(success).to.equal(true);

      expect(errors).to.equal(null);
    });
  });
});

// example unit tests
/* eslint dot-notation: 0 */
'use strict';

describe('Model Deck', () => {
  let expect, DeckModel = require('../deck.js'),
    co;
  //data
  const wrong_deck = {
    timestamp: {},
    user: 'me',
    description: {},
    language: {},
    translation: {
      source: null
    },
    lastUpdate: 145684154485000000000000000000000001,
    revisions: [],
    tags: [{}, 12, '', new Date()]
  };
  const wrong_deck2 = {
    revisions: [{
      contentItems: [{
        order: {},
        kind: 'blub',
        ref: {
          id: 'not me',
          revision: {}
        }
      }]
    }],
    tags: []
  };
  const wrong_deck3 = {
    revisions: [{
      //id: null,
      title: {},
      timestamp: {},
      user: 23847928374892374,
      parent: null,
      popularity: {
        0: 1
      },
      theme: 'abc',
      transition: {
        default: -12
      },
      comment: {},
      abstract: {},
      footer: {
        nope: true,
        text: {}
      },
      license: 'for free',
      isFeatured: 'yes',
      priority: 'f',
      language: {},
      translation: {
        status: 'new',
        source: null
      },
      tags: [{}, 12, '', new Date()],
      preferences: null,
      dataSources: [
        {},
        null
      ]
    }]
  };
  const correct_simplest_deck = {
    user: ('4edd40c86762e0fb12000003'),
    timestamp: (new Date()).toString()
  };
  const correct_deck = {
    timestamp: '2016-01-01 01:01:01',
    user: '4edd40c86762e0fb12000003',
    description: 'test',
    language: 'en',
    translation: {
      source: null
    },
    lastUpdate: (new Date()).toString(),
    contentItems: [],
    revisions: [],
    tags: ['test', 'unit', 'blub'],
    0: true,
    kurt: 'kurt'
  };
  const correct_deck2 = {
    timestamp: '2016-01-01 01:01:01',
    user: '4edd40c86762e0fb12000003',
    description: 'test',
    language: 'en',
    translation: {
      source: null
    },
    lastUpdate: (new Date()).toString(),
    contentItems: [],
    revisions: [],
    tags: ['test', 'unit', 'blub'],
    revisions: [{
      id: 1,
      timestamp: (new Date()).toString(),
      user: '4edd40c86762e0fb12000003',
      license: 'CC0',
      contentItems: [{
        order: '1-1',
        kind: 'slide',
        ref: {
          id: '4edd40c86762e0fb12000005',
          revision: 1
        }
      }],
      dataSources: [
        '4edd40c86762e0fb12000099'
      ]
    }]
  };

  //get modules
  beforeEach(() => {
    require('chai').should();
    expect = require('chai').expect;
    co = require('../common.js');
  });

  context('when validating wrong decks it', () => {
    it('should return errors with the wrong attributes', () => {
      const success = DeckModel(wrong_deck);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(DeckModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong.tags).to.not.equal(undefined);
      expect(errors.wrong.lastUpdate).to.not.equal(undefined);
      expect(errors.wrong.language).to.not.equal(undefined);
      expect(errors.wrong.description).to.not.equal(undefined);
      expect(errors.wrong.user).to.not.equal(undefined);
      expect(errors.wrong.timestamp).to.not.equal(undefined);
    });

    it('should return errors with the wrong attributes #2', () => {
      const success = DeckModel(wrong_deck2);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(DeckModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.missing.user).to.not.equal(undefined);
      expect(errors.missing.timestamp).to.not.equal(undefined);
      expect(errors.missing.id).to.not.equal(undefined);
      expect(errors.missing.license).to.not.equal(undefined);

      expect(errors.wrong['revisions[0].contentItems[0].order']).to.not.equal(undefined);
      expect(errors.wrong['revisions[0].contentItems[0].kind']).to.not.equal(undefined);
      expect(errors.wrong['revisions[0].contentItems[0].ref.revision']).to.not.equal(undefined);
      expect(errors.wrong['revisions[0].contentItems[0].ref.id']).to.not.equal(undefined);
    });

    it('should return errors with the wrong attributes #3', () => {
      const success = DeckModel(wrong_deck3);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(DeckModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.missing.user).to.not.equal(undefined);
      expect(errors.missing.timestamp).to.not.equal(undefined);
      //expect(errors.missing.license).to.not.equal(undefined);
      expect(errors.missing.id).to.not.equal(undefined);
      expect(errors.missing.id.dataPath).to.equal('.revisions[0]');

      expect(errors.wrong['revisions[0].title']).to.not.equal(undefined);
      expect(errors.wrong['revisions[0].timestamp']).to.not.equal(undefined);
      expect(errors.wrong['revisions[0].user']).to.not.equal(undefined);
      expect(errors.wrong['revisions[0].popularity']).to.not.equal(undefined);
      expect(errors.wrong['revisions[0].comment']).to.not.equal(undefined);
      expect(errors.wrong['revisions[0].abstract']).to.not.equal(undefined);
      expect(errors.wrong['revisions[0].license']).to.not.equal(undefined);
      expect(errors.wrong['revisions[0].isFeatured']).to.not.equal(undefined);
      expect(errors.wrong['revisions[0].language']).to.not.equal(undefined);
      expect(errors.wrong['revisions[0].transition.default']).to.not.equal(undefined);
      expect(errors.wrong['revisions[0].translation.status']).to.not.equal(undefined);

      //not:
      //expect(errors.wrong['revisions[0].tags']).to.not.equal(undefined);
      //instead:
      expect(errors.wrong.revisions).to.not.equal(undefined);
      expect(errors.wrong.revisions.occurences).to.equal(5);
    });
  });

  context('when validating correct decks it', () => {
    it('should validate without errors', () => {
      const success = DeckModel(correct_simplest_deck);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(DeckModel.errors);

      //console.log(errors);

      expect(success).to.equal(true);

      expect(errors).to.equal(null);
    });

    it('should validate without errors #2', () => {
      const success = DeckModel(correct_deck);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(DeckModel.errors);

      //console.log(errors);

      expect(success).to.equal(true);

      expect(errors).to.equal(null);
    });

    it('should validate without errors #3', () => {
      const success = DeckModel(correct_deck2);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(DeckModel.errors);

      //console.log(errors);

      expect(success).to.equal(true);

      expect(errors).to.equal(null);
    });
  });
});

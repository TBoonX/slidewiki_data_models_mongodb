// example unit tests
/* eslint dot-notation: 0 */
'use strict';

describe('Model Transition', () => {
  let expect, TransitionModel = require('../transition.js'),
    co;

  //data
  const wrong_transition = {
    scss: {
      varfunc: 0,
      content: false
    },
    timestamp: 0
  };
  const slightlyWrongTransition = {
    user: '112233445566778899101112',
    name: 'awesome'+12,
    css: {
      varfunc: '',
      content: ''
    },
    scss: 'display: none;',
    timestamp: (new Date()).toString(),
    parent: '112233445566778899101112',
    cronjobs: [1, {}]
  };
  const correctTransition = {
    user: '112233445566778899101112',
    name: 'awesome',
    scss: {
      varfunc: '',
      content: ''
    },
    css: 'display: none;',
    timestamp: (new Date()).toString(),
    parent: '112233445566778899101112',
    cronjobs: [{}, {}]
  };

  //get modules
  beforeEach(() => {
    require('chai').should();
    expect = require('chai').expect;
    co = require('../common.js');
  });

  context('when validating wrong messages it', () => {
    it('should return an error with the wrong elements', () => {
      const success = TransitionModel(wrong_transition);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(TransitionModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong['scss.varfunc']).to.not.equal(undefined);
      expect(errors.wrong['scss.content']).to.not.equal(undefined);
      expect(errors.wrong.timestamp).to.not.equal(undefined);

      expect(errors.missing.user).to.not.equal(undefined);
    });

    it('should return an error with the wrong elements #2', () => {
      const success = TransitionModel(slightlyWrongTransition);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(TransitionModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong.css).to.not.equal(undefined);
      expect(errors.wrong.scss).to.not.equal(undefined);
      expect(errors.wrong['cronjobs']).to.not.equal(undefined);
    });
  });

  context('when validating correct messages', () => {
    it('should validate without errors', () => {
      const success = TransitionModel(correctTransition);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(TransitionModel.errors);

      //console.log(errors);

      expect(success).to.equal(true);

      expect(errors).to.equal(null);
    });
  });
});

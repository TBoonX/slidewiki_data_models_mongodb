// example unit tests
/* eslint dot-notation: 0 */
'use strict';

describe('Model Testresult', () => {
  let expect, TestresultModel = require('../testresult.js'),
    co;

  //data
  const wrong_testresult = {
    answer: {
      0: []
    }
  };
  const slightlyWrongTestresult = {
    test: '112233445566778899101112',
    question: -12,
    answer: 'yes',
    explanation: 'because at night its hotter than outdoor',
    isRight: 'left'
  };
  const correctTestresult = {
    test: '112233445566778899101112',
    question: 1,
    answer: 'yes',
    explanation: 'because at night its hotter than outdoor',
    isRight: false,
    user: '112233445566778899101112'
  };

  //get modules
  beforeEach(() => {
    require('chai').should();
    expect = require('chai').expect;
    co = require('../common.js');
  });

  context('when validating wrong tests it', () => {
    it('should return an error with the wrong elements', () => {
      const success = TestresultModel(wrong_testresult);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(TestresultModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong.answer).to.not.equal(undefined);

      expect(errors.missing.test).to.not.equal(undefined);
      expect(errors.missing.question).to.not.equal(undefined);
      expect(errors.missing.user).to.not.equal(undefined);
    });

    it('should return an error with the wrong elements #2', () => {
      const success = TestresultModel(slightlyWrongTestresult);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(TestresultModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong.question).to.not.equal(undefined);
      expect(errors.wrong.isRight).to.not.equal(undefined);
      expect(errors.missing.user).to.not.equal(undefined);
    });
  });

  context('when validating correct tests', () => {
    it('should validate without errors', () => {
      const success = TestresultModel(correctTestresult);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(TestresultModel.errors);

      //console.log(errors);

      expect(success).to.equal(true);

      expect(errors).to.equal(null);
    });
  });
});

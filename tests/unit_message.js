// example unit tests
/* eslint dot-notation: 0 */
'use strict';

describe('Model Message', () => {
  let expect, MessageModel = require('../message.js'),
    co;

  //data
  const wrong_message = {
    title: false
  };
  const slightlyWrongMessage = {
    sender: '112233445566778899101112',
    receiver: 112233445566778899101112,
    title: 'title',
    content: [1, 2, 4, 56],
    timestamp: (new Date()).toString(),
    type: 'editorships'
  };
  const correctMessage = {
    sender: '112233445566778899101112',
    receiver: '112233445566778899101112',
    title: 'title',
    content: 'content',
    timestamp: (new Date()).toString(),
    type: 'pm'
  };

  //get modules
  beforeEach(() => {
    require('chai').should();
    expect = require('chai').expect;
    co = require('../common.js');
  });

  context('when validating wrong messages it', () => {
    it('should return an error with the wrong elements', () => {
      const success = MessageModel(wrong_message);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(MessageModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong.title).to.not.equal(undefined);

      expect(errors.missing.sender).to.not.equal(undefined);
      expect(errors.missing.receiver).to.not.equal(undefined);
      expect(errors.missing.content).to.not.equal(undefined);
      expect(errors.missing.timestamp).to.not.equal(undefined);
      expect(errors.missing.type).to.not.equal(undefined);
    });

    it('should return an error with the wrong elements #2', () => {
      const success = MessageModel(slightlyWrongMessage);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(MessageModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong.receiver).to.not.equal(undefined);
      expect(errors.wrong.content).to.not.equal(undefined);
      expect(errors.wrong.type).to.not.equal(undefined);
    });
  });

  context('when validating correct messages', () => {
    it('should validate without errors', () => {
      const success = MessageModel(correctMessage);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(MessageModel.errors);

      //console.log(errors);

      expect(success).to.equal(true);

      expect(errors).to.equal(null);
    });
  });
});

// example unit tests
/* eslint dot-notation: 0 */
'use strict';

describe('Model Media', () => {
  let expect, MediaModel = require('../media.js'),
    co;

  //data
  const wrong_media = {
    user: {
      width: '12',
      height: 33
    },
    references: [{
      id: 1122334455667788,
      revision: false
    }],
    URI: function(){console.log('Nope!');}
  };
  const slightlyWrongMedia = {
    user: '112233445566778899101112',
    type: ['image'],
    URI: 'http://example.com/image1.png',
    timestamp: new Date(),
    title: 'title',
    original: {
      width: '12',
      height: 33
    },
    content: '',
    references: [{
      id: '112233445566778899101112',
      revision: false
    }]
  };
  const correctMedia = {
    user: '112233445566778899101112',
    type: 'image',
    URI: 'http://example.com/image1.png',
    timestamp: (new Date()).toString(),
    title: 'title',
    original: {
      width: 12,
      height: 33
    },
    content: {},
    references: [{
      id: '112233445566778899101112',
      revision: 189
    }]
  };

  //get modules
  beforeEach(() => {
    require('chai').should();
    expect = require('chai').expect;
    co = require('../common.js');
  });

  context('when validating wrong media it', () => {
    it('should return an error with the wrong elements', () => {
      const success = MediaModel(wrong_media);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(MediaModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong.URI).to.not.equal(undefined);
      expect(errors.wrong.user).to.not.equal(undefined);
      expect(errors.wrong['references[0].id']).to.not.equal(undefined);
      expect(errors.wrong['references[0].revision']).to.not.equal(undefined);

      expect(errors.missing.type).to.not.equal(undefined);
      expect(errors.missing.timestamp).to.not.equal(undefined);
    });

    it('should return an error with the wrong elements #2', () => {
      const success = MediaModel(slightlyWrongMedia);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(MediaModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong.type).to.not.equal(undefined);
      expect(errors.wrong.timestamp).to.not.equal(undefined);
      expect(errors.wrong.content).to.not.equal(undefined);
      expect(errors.wrong['original.width']).to.not.equal(undefined);
      expect(errors.wrong['references[0].revision']).to.not.equal(undefined);
    });
  });

  context('when validating correct media', () => {
    it('should validate without errors', () => {
      const success = MediaModel(correctMedia);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(MediaModel.errors);

      //console.log(errors);

      expect(success).to.equal(true);

      expect(errors).to.equal(null);
    });
  });
});

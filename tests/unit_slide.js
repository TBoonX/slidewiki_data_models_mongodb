// example unit tests
/* eslint dot-notation: 0 */
'use strict';

describe('Model Slide', () => {
  let expect, SlideModel = require('../slide.js'),
    co;

  //data
  const correct_simplest_slide = {
    user: ('4edd40c86762e0fb12000003'),
    deck: ('4edd40c86762e0fb12000004'),
    description: 'test',
    'timestamp': (new Date()).toString()
  };
  const wrong_slide = {
    description: {
      nope: true
    },
    tags: [{
      nope: true
    }]
  };
  const wrong_slide2 = {
    user: ('4edd40c86762e0fb12000003'),
    deck: ('4edd40c86762e0fb12000004'),
    description: 'test',
    language: {
      nope: true
    },
    position: false,
    revisions: [],
    contributers: [],
    tags: ['test', 33, {
      nope: true
    }]
  };
  const wrong_slide3 = {
    user: ('4edd40c86762e0fb12000003'),
    deck: ('4edd40c86762e0fb12000004'),
    description: 'test',
    revisions: [1, 'blub', {
      nope: true
    }]
  };
  const wrong_slide4 = {
    user: ('4edd40c86762e0fb12000003'),
    deck: ('4edd40c86762e0fb12000004'),
    description: 'test',
    revisions: [{
      id: 1,
      title: 'test2',
      user: null,
      license: 'nope',
      tags: [],
      dataSources: [{},
        null
      ]
    }]
  };
  const wrong_slide5 = {
    user: ('4edd40c86762e0fb12000003'),
    deck: ('4edd40c86762e0fb12000004'),
    description: 'test',
    contributers: [1, 'blub', {
      nope: true
    }]
  };
  const wrong_slide6 = {
    user: ('4edd40c86762e0fb12000003'),
    deck: ('4edd40c86762e0fb12000004'),
    description: 'test',
    timestamp: (new Date()).toString(),
    contributers: [{
      id: 'fasdas',
      name: {
        nope: true
      }
    }]
  };

  const correct_slide2 = {
    user: ('4edd40c86762e0fb12000003'),
    deck: ('4edd40c86762e0fb12000004'),
    description: 'test',
    language: 'en',
    timestamp: (new Date()).toString(),
    translation: {
      source: {
        id: '4edd40c86762e0fb12000005',
        revision: 2
      }
    },
    position: 1,
    revisions: [{
      id: 1,
      title: 'test',
      timestamp: (new Date()).toString(),
      user: '4edd40c86762e0fb12000003',
      parent: {},
      popularity: 12,
      comment: '',
      note: '',
      license: 'CC BY',
      translation: {
        status: 'original'
      },
      tags: ['test', 'unit'],
      media: [],
      dataSources: [
        '4edd40c86762e0fb12000099'
      ]
    }],
    contributers: [{
      id: '4edd40c86762e0fb12000003',
      name: 'Kurt'
    }],
    tags: ['test', 'unit', 'dummy']
  };

  const slightly_correct_slide = {
    user: ('4edd40c86762e0fb12000003'),
    deck: ('4edd40c86762e0fb12000004'),
    description: 'test',
    language: 'en',
    timestamp: (new Date()).toString(),
    translation: {
      source: {
        id: '4edd40c86762e0fb12000005',
        revision: 2
      }
    },
    position: 1,
    revisions: [{
      id: 1,
      title: 'test',
      timestamp: (new Date()).toString(),
      user: '4edd40c86762e0fb12000003',
      license: 'CC BY'
    }, {
      id: 1,
      title: 'test',
      timestamp: (new Date()).toString(),
      user: '4edd40c86762e0fb12000003',
      license: 'CC BY'
    }, {
      id: 2,
      title: 'test',
      timestamp: (new Date()).toString(),
      user: '4edd40c86762e0fb12000003',
      license: 'CC BY'
    }, {
      id: 2,
      title: 'test',
      timestamp: (new Date()).toString(),
      user: '4edd40c86762e0fb12000003',
      license: 'CC BY'
    }, {
      id: 1,
      title: 'test',
      timestamp: (new Date()).toString(),
      user: '4edd40c86762e0fb12000003',
      license: 'CC BY'
    }],
    contributers: [{
      id: '4edd40c86762e0fb12000003',
      name: 'Kurt'
    }],
    tags: ['test', 'unit', 'dummy']
  };

  //get modules
  beforeEach(() => {
    require('chai').should();
    expect = require('chai').expect;
    co = require('../common.js');
  });

  context('when validate wrong slides it', () => {
    it('should return an error with the wrong elements', () => {
      const success = SlideModel(wrong_slide);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(SlideModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.missing.deck).to.not.equal(undefined);
      expect(errors.missing.user).to.not.equal(undefined);
      expect(errors.missing.timestamp).to.not.equal(undefined);

      expect(errors.wrong.tags).to.not.equal(undefined);
      expect(errors.wrong.description).to.not.equal(undefined);
    });

    it('should return an error with the wrong elements #2', () => {
      const success = SlideModel(wrong_slide2);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(SlideModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.missing.timestamp).to.not.equal(undefined);

      expect(errors.wrong.tags).to.not.equal(undefined);
      expect(errors.wrong.position).to.not.equal(undefined);
      expect(errors.wrong.language).to.not.equal(undefined);
    });

    it('should return an error with the wrong elements #3', () => {
      const success = SlideModel(wrong_slide3);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(SlideModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.missing.timestamp).to.not.equal(undefined);

      expect(errors.wrong.revisions).to.not.equal(undefined);
    });

    it('should return an error with the wrong elements #4', () => {
      const success = SlideModel(wrong_slide4);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(SlideModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.missing.timestamp).to.not.equal(undefined);

      expect(errors.wrong['revisions[0].license']).to.not.equal(undefined);
      expect(errors.wrong['revisions[0].user']).to.not.equal(undefined);

      expect(errors.wrong.revisions).to.not.equal(undefined);
      expect(errors.wrong.revisions.dataPath).to.equal('.revisions[0].dataSources[0]');
    });

    it('should return an error with the wrong elements #5', () => {
      const success = SlideModel(wrong_slide5);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(SlideModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.missing.timestamp).to.not.equal(undefined);

      expect(errors.wrong.contributers).to.not.equal(undefined);
    });

    it('should return an error with the wrong elements #6', () => {
      const success = SlideModel(wrong_slide6);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(SlideModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong['contributers[0].name']).to.not.equal(undefined);
      expect(errors.wrong['contributers[0].id']).to.not.equal(undefined);
    });

    it('should return an error with the wrong elements/keywords #7', () => {
      const success = SlideModel(slightly_correct_slide);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(SlideModel.errors);

      //console.log(SlideModel.errors);
      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong['#/validateRevisionIds with key id1']).to.not.equal(undefined);
      expect(errors.wrong['#/validateRevisionIds with key id2']).to.not.equal(undefined);
    });
  });

  context('when validating correct slides it', () => {
    it('should validate without errors', () => {
      const success = SlideModel(correct_simplest_slide);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(SlideModel.errors);

      expect(success).to.equal(true);

      expect(errors).to.equal(null);
    });

    it('should validate without errors #2', () => {
      const success = SlideModel(correct_slide2);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(SlideModel.errors);

      expect(success).to.equal(true);

      expect(errors).to.equal(null);
    });
  });
});

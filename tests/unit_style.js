// example unit tests
/* eslint dot-notation: 0 */
'use strict';

describe('Model Style', () => {
  let expect, StyleModel = require('../style.js'),
    co;

  //data
  const wrong_style = {
    scss: [1,2],
    comment: function(){return 'dummy';}
  };
  const slightlyWrongStyle = {
    user: '112233445566778899101112',
    name: 'name',
    scss: {
      varfunc: function() {
        console.log(null);
      },
      content: 'content'
    },
    css: 'color:blue;',
    timestamp: (new Date()).toString(),
    comment: 'blub',
    parent: 'mumumu'
  };
  const correctStyle = {
    user: '112233445566778899101112',
    name: 'name',
    scss: {
      varfunc: '',
      content: 'content'
    },
    css: 'color:blue;',
    timestamp: (new Date()).toString(),
    comment: 'blub',
    parent: {}
  };

  //get modules
  beforeEach(() => {
    require('chai').should();
    expect = require('chai').expect;
    co = require('../common.js');
  });

  context('when validating wrong styles it', () => {
    it('should return an error with the wrong elements', () => {
      const success = StyleModel(wrong_style);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(StyleModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong.scss).to.not.equal(undefined);
      expect(errors.wrong.comment).to.not.equal(undefined);

      expect(errors.missing.user).to.not.equal(undefined);
      expect(errors.missing.timestamp).to.not.equal(undefined);
    });

    it('should return an error with the wrong elements #2', () => {
      const success = StyleModel(slightlyWrongStyle);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(StyleModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong.parent).to.not.equal(undefined);
      expect(errors.wrong['scss.varfunc']).to.not.equal(undefined);
    });
  });

  context('when validating correct styles', () => {
    it('should validate without errors', () => {
      const success = StyleModel(correctStyle);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(StyleModel.errors);

      //console.log(errors);

      expect(success).to.equal(true);

      expect(errors).to.equal(null);
    });
  });
});

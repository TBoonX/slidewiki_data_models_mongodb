// example unit tests
/* eslint dot-notation: 0 */
'use strict';

describe('Model Test', () => {
  let expect, TestModel = require('../test.js'),
    co;

  //data
  const wrong_test = {
    no: true,
    questions: {
      0: {
        id: 0,
        question: {},
        user: '112233445566778899101112',
        timestamp: (new Date()).toString()
      }
    }
  };
  const slightlyWrongTest = {
    user: '112233445566778899101112',
    title: 'title',
    timestamp: (new Date()).toString(),
    maxPoints: 10,
    type: 'list',
    limit: '',
    questions: [{
      id: -11,
      question: 'Who am I?',
      difficulty: 'impossible',
      points: 1,
      user: '112233445566778899101112',
      parent: '112233445566778899101112',
      timestamp: (new Date()).toString(),
      mark: 'accepted',
      answers: {
        count: -4
      },
      solution: {
        text: 'me'
      }
    }]
  };
  const correctTest = {
    user: '112233445566778899101112',
    title: 'title',
    timestamp: (new Date()).toString(),
    maxPoints: 1,
    type: 'list',
    limit: 4,
    questions: [{
      id: 1,
      question: 'Who am I?',
      difficulty: 'impossbile',
      points: 1,
      user: '112233445566778899101112',
      parent: '112233445566778899101112',
      timestamp: (new Date()).toString(),
      mark: 'accepted',
      answers: {
        count: 0
      },
      solution: {
        text: 'me'
      }
    }]
  };

  //get modules
  beforeEach(() => {
    require('chai').should();
    expect = require('chai').expect;
    co = require('../common.js');
  });

  context('when validating wrong tests it', () => {
    it('should return an error with the wrong elements', () => {
      const success = TestModel(wrong_test);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(TestModel.errors);

      //console.log(TestModel.errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong.questions).to.not.equal(undefined);

      expect(errors.missing.user).to.not.equal(undefined);
      expect(errors.missing.timestamp).to.not.equal(undefined);
      expect(errors.missing.type).to.not.equal(undefined);
    });

    it('should return an error with the wrong elements #2', () => {
      const success = TestModel(slightlyWrongTest);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(TestModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong.limit).to.not.equal(undefined);
      expect(errors.wrong['questions[0].id']).to.not.equal(undefined);
      expect(errors.wrong['questions[0].answers.count']).to.not.equal(undefined);
      //expect(errors.wrong['#/validateMaxPoints']).to.not.equal(undefined); //only visible if there are no other errors
    });
  });

  context('when validating correct tests', () => {
    it('should validate without errors', () => {
      const success = TestModel(correctTest);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(TestModel.errors);

      //console.log(errors);

      expect(success).to.equal(true);

      expect(errors).to.equal(null);
    });
  });
});

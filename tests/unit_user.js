// example unit tests
/* eslint dot-notation: 0 */
'use strict';

describe('Model User', () => {
  let expect, UserModel = require('../user.js'), co;

  //data
  const wrong_user = {
    email: {},
    username: {},
    password: {},
    registered: 2349832948329483294823948923849,
    defaults: null,
    firstName: {},
    lastName: {},
    gender: 'normal',
    locale: {},
    hometown: {},
    location: {},
    languages: ['en', 1, {}],
    picture: {},
    interests: {},
    description: {},
    birthday: {
      year: {},
      month: {},
      day: '1 2'
    },
    infodeck: null
  };
  const slightlyWrongUser = {
    email: 'a@sdf.de@de',
    birthday: {
      year: 2016,
      month: 3
    },
    username: '',
    registered: (new Date()).toString()
  };
  const correctUser = {
    email: 'k-j@a-sd.de',
    username: 'kj',
    password: 'sidfu9msdfu08',
    registered: (new Date()).toString(),
    defaults: [{
      color: '#122345'
    }],
    firstName: 'Kurt',
    lastName: 'Junghanns',
    gender: 'male',
    locale: 'DE_de',
    hometown: 'Leipzig',
    location: 'Augusteum 10\n04109 Leipzig\nGermany',
    languages: ['en', 'de'],
    picture: '',
    interests: 'slidewiki, computer, life',
    description: 'I am as awesome as I need to',
    birthday: {
      year: 2016,
      month: 2,
      day: 1
    },
    infodeck: '112233445566778899101112'
  };

  //get modules
  beforeEach(() => {
    require('chai').should();
    expect = require('chai').expect;
    co = require('../common.js');
  });

  context('when validating wrong users it', () => {
    it('should return an error with the wrong elements', () => {
      const success = UserModel(wrong_user);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(UserModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong.email).to.not.equal(undefined);
      expect(errors.wrong.username).to.not.equal(undefined);
      expect(errors.wrong.registered).to.not.equal(undefined);
      expect(errors.wrong.firstName).to.not.equal(undefined);
      expect(errors.wrong.lastName).to.not.equal(undefined);
      expect(errors.wrong.gender).to.not.equal(undefined);
      expect(errors.wrong.locale).to.not.equal(undefined);
      expect(errors.wrong.hometown).to.not.equal(undefined);
      expect(errors.wrong.location).to.not.equal(undefined);
      expect(errors.wrong.languages).to.not.equal(undefined);
      expect(errors.wrong.picture).to.not.equal(undefined);
      expect(errors.wrong.interests).to.not.equal(undefined);
      expect(errors.wrong.description).to.not.equal(undefined);
      expect(errors.wrong['birthday.year']).to.not.equal(undefined);
      expect(errors.wrong['birthday.month']).to.not.equal(undefined);
      expect(errors.wrong['birthday.day']).to.not.equal(undefined);
    });

    it('should return an error with the wrong elements #2', () => {
      const success = UserModel(slightlyWrongUser);

      expect(success).to.equal(false);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(UserModel.errors);

      //console.log(errors);

      expect(errors).to.not.equal(null);

      expect(errors.wrong.email).to.not.equal(undefined);
    });
  });

  context('when validating correct users', () => {
    it('should validate without errors', () => {
      const success = UserModel(correctUser);

      //we got an object like this: {missing: {attribute: {}, ...}, wrong: {attribute2: {}}}
      const errors = co.parseAjvValidationErrors(UserModel.errors);

      //console.log(errors);

      expect(success).to.equal(true);

      expect(errors).to.equal(null);
    });
  });
});

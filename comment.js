'use strict';

//require
var Ajv = require('ajv');
var ajv = Ajv({
  verbose: true,
  allErrors: true
    //v5: true  //enable v5 proposal of JSON-schema standard
}); // options can be passed, e.g. {allErrors: true}

//build schema
const objectid = {
  type: 'string',
  maxLength: 24,
  minLength: 24
};
const comment = {
  type: 'object',
  properties: {
    user: objectid,
    item: {
      type: 'object',
      properties: {
        kind: {
          type: 'string',
          enum: ['deck', 'slide', 'comment', 'user']
        },
        ref: {
          type: 'object',
          properties: {
            id: objectid,
            revision: {
              type: 'number'
            } //if not given use the last revision
          },
          required: ['id']
        }
      },
      required: ['kind']
    },
    title: {
      type: 'string'
    },
    text: {
      type: 'string'
    },
    timestamp: {
      type: 'string'
    }
  },
  required: ['user', 'item', 'timestamp']
};

//export
module.exports = ajv.compile(comment);

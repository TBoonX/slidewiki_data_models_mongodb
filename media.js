'use strict';

//require
var Ajv = require('ajv');
var ajv = Ajv({
  verbose: true,
  allErrors: true
    //v5: true  //enable v5 proposal of JSON-schema standard
}); // options can be passed, e.g. {allErrors: true}

//build schema
const objectid = {
  type: 'string',
  maxLength: 24,
  minLength: 24
};
const reference = {
  type: 'object',
  properties: {
    id: objectid,
    revision: {
      type: 'number',
      minimum: 1
    } //if not given use the last revision
  },
  required: ['id']
};
const media = {
  type: 'object',
  properties: {
    user: objectid,
    type: {
      type: 'string'
    },
    URI: {
      type: 'string'
    },
    timestamp: {
      type: 'string'
    },
    title: {
      type: 'string'
    },
    original: {
      type: 'object',
      properties: {
        width: {
          type: 'number'
        },
        height: {
          type: 'number'
        }
      }
    },
    content: {
      type: 'object'
    },
    references: {
      type: 'array',
      items: reference
    }
  },
  required: ['user', 'type', 'timestamp']
};

//export
module.exports = ajv.compile(media);

'use strict';

//require
var Ajv = require('ajv');
var ajv = Ajv({
  verbose: true,
  allErrors: true
    //v5: true  //enable v5 proposal of JSON-schema standard
}); // options can be passed, e.g. {allErrors: true}

//build schema
const objectid = {
  type: 'string',
  maxLength: 24,
  minLength: 24
};
//keyword for summarize points of questions and compare the result with the maxPoints attribute
//it will create an error if maxPoint is wrong and no other errors occured
ajv.addKeyword('validateMaxPoints', {
  errors: true,
  async: false,
  validate: function myValidation(value, dataObject) {
    if (!value)
      return true;

    if (dataObject.questions === undefined || Object.prototype.toString.call(dataObject.questions) !== '[object Array]')
      return dataObject.maxPoints === 0;

    let pointCount = 0;
    dataObject.questions.forEach((question) => {
      pointCount += question.points;
    });

    const maxPoints = dataObject.maxPoints;

    if (maxPoints === pointCount)
      return true;

    if (myValidation.errors === null)
      myValidation.errors = [];

    myValidation.errors.push({
      keyword: 'validateMaxPoints',
      message: 'maxPoints attribute should be ' + pointCount + ', but is ' + maxPoints,
      params: {
        keyword: 'validateMaxPoints'
      }/*,
      dataPath: '.maxPoints',
      schemaPath: '#/validateMaxPoints',
      schema: true*/
    });

    return false;
  }
});
const question = {
  type: 'object',
  properties: {
    id: { //increment with every new question
      type: 'number',
      minimum: 1
    },
    question: {
      type: 'string'
    },
    difficulty: {
      type: 'string'
    },
    points: {
      type: 'number'
    },
    user: objectid,
    parent: objectid,
    timestamp: {
      type: 'string'
    },
    mark: {
      type: 'string',
      enum: ['suggested', 'doubtful', 'accepted']
    },
    answers: {
      type: 'object',
      properties: {
        count: {
          type: 'number',
          minimum: 0
        }
      }
    },
    solution: {
      type: 'object'
    }
  },
  required: ['id', 'question', 'user', 'timestamp', 'points']
};
const test = {
  type: 'object',
  properties: {
    user: objectid,
    title: {
      type: 'string'
    },
    timestamp: {
      type: 'string'
    },
    maxPoints: {
      type: 'number'
    },
    type: {
      type: 'string',
      enum: ['auto', 'list', 'exam']
    },
    limit: {
      type: 'number'
    },
    questions: {
      type: 'array',
      items: question
    }
  },
  required: ['user', 'timestamp', 'type'],
  validateMaxPoints: true
};

//export
module.exports = ajv.compile(test);
